FROM openjdk:8
EXPOSE 8080
ADD /target/UserService-0.0.1.jar UserService.jar
ENTRYPOINT ["java","-jar","UserService.jar"]