# CREATE TABLE author (
#   id         BIGINT       NOT NULL AUTO_INCREMENT,
#   first_name VARCHAR(255) NOT NULL,
#   PRIMARY KEY (id)
# );
# CREATE TABLE authority (
#   id   BIGINT       NOT NULL AUTO_INCREMENT,
#   name VARCHAR(255) NOT NULL,
#   PRIMARY KEY (id)
# );
# CREATE TABLE book (
#   id    BIGINT       NOT NULL AUTO_INCREMENT,
#   title VARCHAR(255) NOT NULL,
#   PRIMARY KEY (id)
# );
# CREATE TABLE book_authors (
#   book_id    BIGINT NOT NULL,
#   authors_id BIGINT NOT NULL,
#   PRIMARY KEY (book_id, authors_id)
# );
# CREATE TABLE comment (
#   id      BIGINT NOT NULL AUTO_INCREMENT,
#   text    VARCHAR(255),
#   book_id BIGINT,
#   PRIMARY KEY (id)
# );
# CREATE TABLE user (
#   id       BIGINT       NOT NULL AUTO_INCREMENT,
#   email    VARCHAR(50),
#   enabled  BIT          NOT NULL,
#   password VARCHAR(100) NOT NULL,
#   username VARCHAR(50)  NOT NULL,
#   PRIMARY KEY (id)
# );
# CREATE TABLE user_authority (
#   user_id      BIGINT NOT NULL,
#   authority_id BIGINT NOT NULL
# );
#
#
# ALTER TABLE user
#   ADD CONSTRAINT UK_sb8bbouer5wak8vyiiy4pf2bx UNIQUE (username);
# ALTER TABLE book_authors
#   ADD CONSTRAINT FK551i3sllw1wj7ex6nir16blsm FOREIGN KEY (authors_id) REFERENCES author (id);
# ALTER TABLE book_authors
#   ADD CONSTRAINT FKs4xm7q8i3uxvaiswj1c35nnxw FOREIGN KEY (book_id) REFERENCES book (id);
# ALTER TABLE comment
#   ADD CONSTRAINT FKkko96rdq8d82wm91vh2jsfak7 FOREIGN KEY (book_id) REFERENCES book (id);
# ALTER TABLE user_authority
#   ADD CONSTRAINT FKgvxjs381k6f48d5d2yi11uh89 FOREIGN KEY (authority_id) REFERENCES authority (id);
# ALTER TABLE user_authority
#   ADD CONSTRAINT FKpqlsjpkybgos9w2svcri7j8xy FOREIGN KEY (user_id) REFERENCES user (id);
#
#
# INSERT INTO authority VALUES (1, 'My Test AAA');
# INSERT INTO user VALUES (1, 'user1@gmail.com', 1, 'passsaaaa', 'user1');