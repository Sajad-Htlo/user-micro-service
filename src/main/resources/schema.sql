CREATE DATABASE IF NOT EXISTS test;

# If table exists, drop it's foreign key before deleting the table itself.

SET @var = if((SELECT TRUE
               FROM information_schema.TABLE_CONSTRAINTS
               WHERE
                 CONSTRAINT_SCHEMA = DATABASE() AND
                 TABLE_NAME = 'book_authors' AND
                 CONSTRAINT_NAME = 'FK551i3sllw1wj7ex6nir16blsm' AND
                 CONSTRAINT_TYPE = 'FOREIGN KEY') = TRUE, 'ALTER TABLE book_authors
            drop foreign key FK551i3sllw1wj7ex6nir16blsm', 'select 1');

PREPARE stmt FROM @var;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @var = if((SELECT TRUE
               FROM information_schema.TABLE_CONSTRAINTS
               WHERE
                 CONSTRAINT_SCHEMA = DATABASE() AND
                 TABLE_NAME = 'book_authors' AND
                 CONSTRAINT_NAME = 'FK551i3sllw1wj7ex6nir16blsm' AND
                 CONSTRAINT_TYPE = 'FOREIGN KEY') = TRUE, 'ALTER TABLE book_authors
            drop foreign key FK551i3sllw1wj7ex6nir16blsm', 'select 1');

PREPARE stmt FROM @var;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @var = if((SELECT TRUE
               FROM information_schema.TABLE_CONSTRAINTS
               WHERE
                 CONSTRAINT_SCHEMA = DATABASE() AND
                 TABLE_NAME = 'book_authors' AND
                 CONSTRAINT_NAME = 'FKs4xm7q8i3uxvaiswj1c35nnxw' AND
                 CONSTRAINT_TYPE = 'FOREIGN KEY') = TRUE, 'ALTER TABLE book_authors
            drop foreign key FKs4xm7q8i3uxvaiswj1c35nnxw', 'select 1');

PREPARE stmt FROM @var;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @var = if((SELECT TRUE
               FROM information_schema.TABLE_CONSTRAINTS
               WHERE
                 CONSTRAINT_SCHEMA = DATABASE() AND
                 TABLE_NAME = 'comment' AND
                 CONSTRAINT_NAME = 'FKkko96rdq8d82wm91vh2jsfak7' AND
                 CONSTRAINT_TYPE = 'FOREIGN KEY') = TRUE, 'ALTER TABLE comment
            drop foreign key FKkko96rdq8d82wm91vh2jsfak7', 'select 1');

PREPARE stmt FROM @var;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @var = if((SELECT TRUE
               FROM information_schema.TABLE_CONSTRAINTS
               WHERE
                 CONSTRAINT_SCHEMA = DATABASE() AND
                 TABLE_NAME = 'user_authority' AND
                 CONSTRAINT_NAME = 'FKgvxjs381k6f48d5d2yi11uh89' AND
                 CONSTRAINT_TYPE = 'FOREIGN KEY') = TRUE, 'ALTER TABLE user_authority
            drop foreign key FKgvxjs381k6f48d5d2yi11uh89', 'select 1');

PREPARE stmt FROM @var;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @var = if((SELECT TRUE
               FROM information_schema.TABLE_CONSTRAINTS
               WHERE
                 CONSTRAINT_SCHEMA = DATABASE() AND
                 TABLE_NAME = 'user_authority' AND
                 CONSTRAINT_NAME = 'FKpqlsjpkybgos9w2svcri7j8xy' AND
                 CONSTRAINT_TYPE = 'FOREIGN KEY') = TRUE, 'ALTER TABLE user_authority
            drop foreign key FKpqlsjpkybgos9w2svcri7j8xy', 'select 1');

PREPARE stmt FROM @var;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

DROP TABLE IF EXISTS author;
DROP TABLE IF EXISTS authority;
DROP TABLE IF EXISTS book;
DROP TABLE IF EXISTS book_authors;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS user_authority;

CREATE TABLE author (
  id         BIGINT       NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE authority (
  id   BIGINT       NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE book (
  id    BIGINT       NOT NULL AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE book_authors (
  book_id    BIGINT NOT NULL,
  authors_id BIGINT NOT NULL,
  PRIMARY KEY (book_id, authors_id)
);
CREATE TABLE comment (
  id      BIGINT NOT NULL AUTO_INCREMENT,
  text    VARCHAR(255),
  book_id BIGINT,
  PRIMARY KEY (id)
);
CREATE TABLE user (
  id       BIGINT       NOT NULL AUTO_INCREMENT,
  email    VARCHAR(50),
  enabled  BIT          NOT NULL,
  password VARCHAR(100) NOT NULL,
  username VARCHAR(50)  NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE user_authority (
  user_id      BIGINT NOT NULL,
  authority_id BIGINT NOT NULL
);

ALTER TABLE user
  ADD CONSTRAINT UK_sb8bbouer5wak8vyiiy4pf2bx UNIQUE (username);
ALTER TABLE book_authors
  ADD CONSTRAINT FK551i3sllw1wj7ex6nir16blsm FOREIGN KEY (authors_id) REFERENCES author (id);
ALTER TABLE book_authors
  ADD CONSTRAINT FKs4xm7q8i3uxvaiswj1c35nnxw FOREIGN KEY (book_id) REFERENCES book (id);
ALTER TABLE comment
  ADD CONSTRAINT FKkko96rdq8d82wm91vh2jsfak7 FOREIGN KEY (book_id) REFERENCES book (id);
ALTER TABLE user_authority
  ADD CONSTRAINT FKgvxjs381k6f48d5d2yi11uh89 FOREIGN KEY (authority_id) REFERENCES authority (id);
ALTER TABLE user_authority
  ADD CONSTRAINT FKpqlsjpkybgos9w2svcri7j8xy FOREIGN KEY (user_id) REFERENCES user (id);