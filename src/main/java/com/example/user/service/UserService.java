package com.example.user.service;

import com.example.user.domain.User;

public interface UserService {


    User getById(long id);

    long persistUser(User user);
}
