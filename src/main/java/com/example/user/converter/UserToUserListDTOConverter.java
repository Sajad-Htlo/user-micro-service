package com.example.user.converter;

import com.example.user.domain.User;
import com.example.user.dto.UserListDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserListDTOConverter implements Converter<User, UserListDTO> {


    @Override
    public UserListDTO convert(User user) {
        UserListDTO listDTO = new UserListDTO();

        listDTO.setId(user.getId());
        listDTO.setUsername(user.getUsername());

        return listDTO;
    }
}
