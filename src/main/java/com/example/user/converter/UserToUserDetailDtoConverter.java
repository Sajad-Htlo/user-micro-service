package com.example.user.converter;

import com.example.user.domain.User;
import com.example.user.dto.UserDetailDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDetailDtoConverter implements Converter<User, UserDetailDto> {

    @Override
    public UserDetailDto convert(User user) {
        UserDetailDto detailDto = new UserDetailDto();

        detailDto.setId(user.getId());
        detailDto.setUsername(user.getUsername());

        return detailDto;
    }
}
